class Car {
    constructor(make, model) {
        this.make = make;
        this.model = model;
    }
    drive() {
        console.log(this.make);
        console.log(this.model);
    }
}
var car1 = new Car("Indonesia", "XX9-TY");
console.log(car1.drive());
console.log(car1 instanceof Car);

class ElectricCar extends Car {
    constructor(make, model,batteryLevel){
        super(make, model);
        this.batteryLevel = batteryLevel;
    }
    drive(e){
        console.log(e);
    }
    charge(){
        console.log(this.batteryLevel);
    }
}
var electricCar1 = new ElectricCar("Indonesia", "XX9-TY",100);
console.log(electricCar1.drive());
console.log(electricCar1.charge());
console.log(electricCar1 instanceof ElectricCar);

class PetrolCar extends Car{
    constructor(make, model,fuelLevel){
        super(make, model);
        this.fuelLevel = fuelLevel;
    }
    drive(){

    }
    fillUp(){
        console.log(this.fuelLevel)
    }
}

var petrolCar1 = new PetrolCar("Indonesia", "XX9-TY",9900);
console.log(petrolCar1.drive());
console.log(petrolCar1.fillUp());
console.log(petrolCar1 instanceof PetrolCar);

